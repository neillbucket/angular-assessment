import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PlaceholderService } from './placeholder.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  //Might change users
  users
  frmId:number = 1
  whichCategory:string = 'people'
  choices:Object[] = [{cat:'people'}, {cat:'planets'}, {cat: 'vehicles'}, {cat:'species'}, {cat:'starships'}]
  model 

  //declare functions
  constructor(private placeholderService:PlaceholderService){
  }

 //handle click function
  handleClick(){
    //invoke the service passing parameters
    this.placeholderService.getParamData(this.frmId, this.whichCategory).subscribe( (result)=>{
      this.model =result
      console.log(result)
    })
  }

  //Call method from our service
  invokeService(){
    this.placeholderService.getData().subscribe( (result) => {
      this.users = result
      console.log(result)
    })
  }

    ngOnInit(){
      //make a call for user data
      this.invokeService()
    }
}
